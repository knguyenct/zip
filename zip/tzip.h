#ifndef TZIP_H
#define TZIP_H

#include <stdio.h>
#include <tchar.h>
#include <Windows.h>

#include "const.h"
#include "struct.h"
#include "tstate.h"
#include "zip_const.h"

class TZip
{
public:
	TZip(const char *pwd);
	
	~TZip();

	uint create(void *z, uint len, uint flags);

	static uint sflush(void *param, const char *buf, uint *size);

	static uint swrite(void *param, const char *buf, uint size);

	uint write(const char *buf, uint size);

	bool oseek(uint pos);

	uint getMemory(void **pbuf, ulong *plen);

	uint addCentral();

	uint close();

	uint openFile(const TCHAR *fn);

	uint openHandle(HANDLE hf, uint len);

	uint openMem(void *src, uint len);
	
	uint openDir();
	
	static uint sread(TState &s, char *buf, uint size);
	
	uint read(char *buf, uint size);
	
	uint iclose();

	uint ideflate(TZipFileInfo *zfi);
	
	uint istore();

	uint add(const TCHAR *odstzn, void *src, uint len, DWORD flags);

private:
	char *password;

	HANDLE hfout;
	
	bool mustclosehfout;

	HANDLE hmapout;

	uint ooffset;

	uint oerr;

	uint writ;

	bool ocanseek;

	char *obuf;

	uint opos;

	uint mapsize;

	bool hasputcen;

	bool encwriting;

	ulong keys[3];

	char *encbuf;

	uint encbufsize;

	TZipFileInfo *zfis;

	TState *state;

	ulong attr;

	iztimes times;
		
	ulong timestamp;  // all open_* methods set these
	
	bool iseekable;
	
	long isize, ired;         // size is not set until close() on pips
	
	ulong crc;                                 // crc is not set until close(). iwrit is cumulative
	
	HANDLE hfin;
	
	bool selfclosehf; // for input files and pipes
	
	const char *bufin;
	
	uint lenin;
	
	uint posin;
	// for memory
	// and a variable for what we've done with the input: (i.e. compressed it!)

	ulong csize;
	// compressed size, set by the compression routines
	// and this is used by some of the compression routines

	char buf[16384];
};

#endif