#include "tstate.h"

TTreeState::TTreeState() {
	tree_desc a = { dyn_ltree, static_ltree, extra_lbits, LITERALS + 1, L_CODES, MAX_BITS, 0 };
	l_desc = a;
	tree_desc b = { dyn_dtree, static_dtree, extra_dbits, 0, D_CODES, MAX_BITS, 0 };
	d_desc = b;
	tree_desc c = { bl_tree, NULL, extra_blbits, 0, BL_CODES, MAX_BL_BITS, 0 };
	bl_desc = c;
	last_lit = 0;
	last_dist = 0;
	last_flags = 0;
}