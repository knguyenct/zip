#ifndef STRUCT_H
#define STRUCT_H

#include <Windows.h>

typedef unsigned char uchar;

typedef unsigned short ushort;

typedef unsigned int uint;

typedef unsigned long ulong;

typedef size_t extent;
// file size

// Data structure describing a single value and its code string.

typedef struct ct_data {

	union {
		ushort freq;
		// frequency count

		ushort code;
		// bit string
	} fc;

	union {
		ushort dad;
		// father node in Huffman tree  

		ushort len;
		// length of bit string
	} dl;
} ct_data;

typedef struct tree_desc {

	ct_data *dyn_tree;
	// the dynamic tree

	ct_data *static_tree;
	// corresponding static tree or NULL

	const int *extra_bits;
	// extra bits for each code or NULL

	int extra_base;
	// base index for extra_bits

	int elems;
	// max number of elements in the tree

	int max_length;
	// max bit length for the codes

	int max_code;
	// largest code with non zero frequency
} tree_desc;

typedef __int64 lutime_t;
// define it ourselves since we don't include time.h

typedef struct iztimes {

	lutime_t atime, mtime, ctime;
} iztimes;
// access, modify, create times

typedef struct zlist {

	ushort vem, ver, flg, how;
	// See central header in zipfile.c for what vem..off are

	ulong tim, crc, siz, len;

	extent nam, ext, cext, com;
	// offset of ext must be >= LOCHEAD

	ushort dsk, att, lflg;
	// offset of lflg must be >= LOCHEAD

	ulong atx, off;

	char name[MAX_PATH];
	// File name in zip file

	char *extra;
	// Extra field (set only if ext != 0)

	char *cextra;
	// Extra in central (set only if cext != 0)

	char *comment;
	// Comment (set only if com != 0)

	char iname[MAX_PATH];
	// Internal file name after cleanup

	char zname[MAX_PATH];
	// External version of internal name

	int mark;
	// Marker for files to operate on

	int trash;
	// Marker for files to delete

	int dosflag;
	// Set to force MSDOS file attributes

	struct zlist *nxt;
	// Pointer to next header in list
} TZipFileInfo;

#endif
