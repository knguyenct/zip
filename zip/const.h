//#ifndef COMMON_H
//#define COMMON_H
//
//// -- ZIP RESULT CODES --
//
//#define ZR_OK         0x00000000
//// nb. the pseudo-code zr-recent is never returned,
//
//#define ZR_RECENT     0x00000001
//// but can be passed to FormatZipMessage.
//
//// The following come from general system stuff (e.g. files not openable)
//#define ZR_GENMASK    0x0000FF00
//
//#define ZR_NODUPH     0x00000100
//// couldn't duplicate the handle
//
//#define ZR_NOFILE     0x00000200
//// couldn't create/open the file
//
//#define ZR_NOALLOC    0x00000300
//// failed to allocate some resource
//
//#define ZR_WRITE      0x00000400
//// a general error writing to the file
//
//#define ZR_NOTFOUND   0x00000500
//// couldn't find that file in the zip
//
//#define ZR_MORE       0x00000600
//// there's still more data to be unzipped
//
//#define ZR_CORRUPT    0x00000700
//// the zipfile is corrupt or not a zipfile
//
//#define ZR_READ       0x00000800
//// a general error reading the file
//
//// The following come from mistakes on the part of the caller
//#define ZR_CALLERMASK 0x00FF0000
//
//#define ZR_ARGS       0x00010000
//// general mistake with the arguments
//
//#define ZR_NOTMMAP    0x00020000
//// tried to ZipGetMemory, but that only works on mmap zipfiles, which yours wasn't
//
//#define ZR_MEMSIZE    0x00030000
//// the memory size is too small
//
//#define ZR_FAILED     0x00040000
//// the thing was already failed when you called this function
//
//#define ZR_ENDED      0x00050000
//// the zip creation has already been closed
//
//#define ZR_MISSIZE    0x00060000
//// the indicated input file size turned out mistaken
//
//#define ZR_PARTIALUNZ 0x00070000
//// the file had already been partially unzipped
//
//#define ZR_ZMODE      0x00080000
//// tried to mix creating/opening a zip 
//
//// The following come from bugs within the zip library itself
//#define ZR_BUGMASK    0xFF000000
//
//#define ZR_NOTINITED  0x01000000
//// initialization didn't work
//
//#define ZR_SEEK       0x02000000
//// trying to seek in an unseekable file
//
//#define ZR_NOCHANGE   0x04000000
//// changed its mind on storage, but not allowed
//
//#define ZR_FLATE      0x05000000
//// an internal error in the de/inflation code
//
//
//// -- ZIP FILE STRUCTURE --
//
//// Signatures for zip file information headers
//#define LOCSIG     0x04034b50L
//
//#define CENSIG     0x02014b50L
//
//#define ENDSIG     0x06054b50L
//
//#define EXTLOCSIG  0x08074b50L
//
//// The minimum and maximum match lengths
//#define MIN_MATCH 3
//
//#define MAX_MATCH 258
//
//#define WSIZE 0x8000
//// Maximum window size = 32K. If you are really short of memory, compile
//// with a smaller WSIZE but this reduces the compression ratio for files
//// of size > WSIZE. WSIZE must be a power of two in the current implementation.
////
//
//#define MAX_BITS 15
///// All codes must not exceed MAX_BITS bits
//
//#define MAX_BL_BITS 7
//// Bit length codes must not exceed MAX_BL_BITS bits
//
//#define LITERALS 256
//// number of literal bytes 0..255

#define LENGTH_CODES 29
// number of length codes, not counting the special END_BLOCK code

//#define L_CODES (LITERALS + 1 + LENGTH_CODES)
//// number of Literal or Length codes, including the END_BLOCK code
//
#define D_CODES 30
// number of distance codes

#define BL_CODES 19

//#define HEAP_SIZE (2 * L_CODES + 1)
//// maximum heap size
//
//#define LIT_BUFSIZE  0x8000
//
//#define DIST_BUFSIZE  LIT_BUFSIZE
//
const int extra_lbits[LENGTH_CODES] = { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0 };
// extra bits for each length code

const int extra_dbits[D_CODES] = { 0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13 };
// extra bits for each distance code

const int extra_blbits[BL_CODES] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 7 };
// extra bits for each bit length code
//
//#define ZIP_HANDLE   1
//
//#define ZIP_FILENAME 2
//
//#define ZIP_MEMORY   3
//
//#define ZIP_FOLDER   4
//
//#endif /* COMMON_H */
//
