#ifndef TSTATE_H
#define TSTATE_H

#include "const.h"
#include "struct.h"

#define MAX_BITS 15

#define MAX_BL_BITS 7
// Bit length codes must not exceed MAX_BL_BITS bits

#define LITERALS 256
// number of literal bytes 0..255

#define L_CODES (LITERALS + 1 + LENGTH_CODES)
// number of Literal or Length codes, including the END_BLOCK code

#define HEAP_SIZE (2 * L_CODES + 1)

#define LIT_BUFSIZE  0x8000

#define DIST_BUFSIZE LIT_BUFSIZE

// The minimum and maximum match lengths
#define MIN_MATCH 3

#define MAX_MATCH 258

#define HASH_BITS  15
// For portability to 16 bit machines, do not use values above 15.

#define HASH_SIZE (unsigned)(1 << HASH_BITS)

#define WSIZE 0x8000

class TTreeState {
public:
	TTreeState();

	ct_data dyn_ltree[HEAP_SIZE];
	// literal and length tree

	ct_data dyn_dtree[2 * D_CODES + 1];
	// distance tree

	ct_data static_ltree[L_CODES + 2];
	// the static literal tree...
	// ... Since the bit lengths are imposed, there is no need for the L_CODES
	// extra codes used during heap construction. However the codes 286 and 287
	// are needed to build a canonical tree (see ct_init below).

	ct_data static_dtree[D_CODES];
	// the static distance tree...
	// ... (Actually a trivial tree since all codes use 5 bits.)

	ct_data bl_tree[2 * BL_CODES + 1];
	// Huffman tree for the bit lengths

	tree_desc l_desc;

	tree_desc d_desc;

	tree_desc bl_desc;

	ushort bl_count[MAX_BITS + 1];
	// number of codes at each bit length for an optimal tree

	int heap[2 * L_CODES + 1];
	// heap used to build the Huffman trees

	int heap_len;
	// number of elements in the heap

	int heap_max;
	// element of largest frequency
	// The sons of heap[n] are heap[2*n] and heap[2*n+1]. heap[0] is not used.
	// The same heap array is used to build all trees.

	uchar depth[2 * L_CODES + 1];
	// Depth of each subtree used as tie breaker for trees of equal frequency

	uchar length_code[MAX_MATCH - MIN_MATCH + 1];
	// length code for each normalized match length (0 == MIN_MATCH)

	uchar dist_code[512];
	// distance codes. The first 256 values correspond to the distances
	// 3 .. 258, the last 256 values correspond to the top 8 bits of
	// the 15 bit distances.

	int base_length[LENGTH_CODES];
	// First normalized length for each code (0 = MIN_MATCH)

	int base_dist[D_CODES];
	// First normalized distance for each code (0 = distance of 1)

	uchar l_buf[LIT_BUFSIZE];
	// buffer for literals/lengths

	uchar d_buf[DIST_BUFSIZE];
	// buffer for distances

	uchar flag_buf[(LIT_BUFSIZE / 8)];
	// flag_buf is a bit array distinguishing literals from lengths in
	// l_buf, and thus indicating the presence or absence of a distance.

	uint last_lit;
	// running index in l_buf

	uint last_dist;
	// running index in d_buf

	uint last_flags;
	// running index in flag_buf
	uint flags;
	// current flags not yet saved in flag_buf

	uchar flag_bit;
	// current bit used in flags
	// bits are filled in flags starting at bit 0 (least significant).
	// Note: these flags are overkill in the current code since we don't
	// take advantage of DIST_BUFSIZE == LIT_BUFSIZE.

	ulong opt_len;
	// bit length of current block with optimal trees

	ulong static_len;
	// bit length of current block with static trees

	ulong cmpr_bytelen;
	// total byte length of compressed file

	ulong cmpr_len_bits;
	// number of bits past 'cmpr_bytelen'

	ulong input_len;
	// total byte length of input file
	// input_len is for debugging only since we can get it by other means.

	ushort *file_type;
	// pointer to UNKNOWN, BINARY or ASCII
	//  int *file_method;
	// pointer to DEFLATE or STORE
};

class TBitState {
public:
	int flush_flg;

	uint bi_buf;
	// Output buffer. bits are inserted starting at the bottom (least significant
	// bits). The width of bi_buf must be at least 16 bits.

	int bi_valid;
	// Number of valid bits in bi_buf.  All bits above the last valid bit
	// are always zero.

	char *out_buf;
	// Current output buffer.

	uint out_offset;
	// Current offset in output buffer.
	// On 16 bit machines, the buffer is limited to 64K.

	uint out_size;
	// Size of current output buffer

	ulong bits_sent;
	// bit length of the compressed data  only needed for debugging???
};

class TDeflateState {
public:

	TDeflateState() {
		window_size = 0;
	}

	uchar window[2L * WSIZE];
	// Sliding window. Input bytes are read into the second half of the window,
	// and move to the first half later to keep a dictionary of at least WSIZE
	// bytes. With this organization, matches are limited to a distance of
	// WSIZE-MAX_MATCH bytes, but this ensures that IO is always
	// performed with a length multiple of the block size. Also, it limits
	// the window size to 64K, which is quite useful on MSDOS.
	// To do: limit the window size to WSIZE+CBSZ if SMALL_MEM (the code would
	// be less efficient since the data would have to be copied WSIZE/CBSZ times)

	uint prev[WSIZE];
	// Link to older string with same hash index. To limit the size of this
	// array to 64K, this link is maintained only for the last 32K strings.
	// An index in this array is thus a window index modulo 32K.

	uint head[HASH_SIZE];
	// Heads of the hash chains or NIL. If your compiler thinks that
	// HASH_SIZE is a dynamic value, recompile with -DDYN_ALLOC.

	ulong window_size;
	// window size, 2*WSIZE except for MMAP or BIG_MEM, where it is the
	// input file length plus MIN_LOOKAHEAD.

	long block_start;
	// window position at the beginning of the current output block. Gets
	// negative when the window is moved backwards.

	int sliding;
	// Set to false when the input file is already in memory

	uint ins_h; // hash index of string to be inserted

	uint prev_length;
	// Length of the best match at previous step. Matches not greater than this
	// are discarded. This is used in the lazy match evaluation.

	uint strstart;
	// start of string to insert

	uint match_start;
	// start of matching string

	int eofile;
	// flag set at end of input file

	uint lookahead;
	// number of valid bytes ahead in window

	uint max_chain_length;
	// To speed up deflation, hash chains are never searched beyond this length.
	// A higher limit improves compression ratio but degrades the speed.

	uint max_lazy_match;
	// Attempt to find a better match only when the current match is strictly
	// smaller than this value. This mechanism is used only for compression
	// levels >= 4.

	uint good_match;
	// Use a faster search when the previous match is longer than this

	int nice_match;
	// Stop searching when current match exceeds this
};

struct TState;

typedef uint(*READFUNC)(TState &state, char *buf, uint size);

typedef uint(*FLUSHFUNC)(void *param, const char *buf, uint *size);

typedef uint(*WRITEFUNC)(void *param, const char *buf, uint size);

struct TState {
	void *param;

	int level;

	bool seekable;

	READFUNC readfunc;

	FLUSHFUNC flush_outbuf;

	TTreeState ts;

	TBitState bs;

	TDeflateState ds;

	const char *err;
};

#endif
