#include "utils.h"

// Macros for writing machine integers to little-endian format
#define PUTSH(a,f) {char _putsh_c=(char)((a)&0xff); wfunc(param,&_putsh_c,1); _putsh_c=(char)((a)>>8); wfunc(param,&_putsh_c,1);}
#define PUTLG(a,f) {PUTSH((a) & 0xffff,(f)) PUTSH((a) >> 16,(f))}

int putend(int n, ulong s, ulong c, extent m, char *z, WRITEFUNC wfunc, void *param)
{ // write the end of the central-directory-data to file *f.
	PUTLG(ENDSIG, f);
	PUTSH(0, f);
	PUTSH(0, f);
	PUTSH(n, f);
	PUTSH(n, f);
	PUTLG(s, f);
	PUTLG(c, f);
	PUTSH(m, f);
	// Write the comment, if any
	if (m && wfunc(param, z, (unsigned int)m) != m) return ZE_TEMP;
	return ZE_OK;
}