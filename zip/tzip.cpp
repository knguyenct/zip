#include "tzip.h"

TZip::TZip(const char *pwd) {
	hfout = 0;
	mustclosehfout = false;
	hmapout = 0;
	zfis = 0;
	obuf = 0;
	hfin = 0;
	writ = 0;
	oerr = false;
	hasputcen = false;
	ooffset = 0;
	encwriting = false;
	encbuf = NULL;
	password = NULL;
	state = 0;
	if (pwd != 0 && *pwd != 0) {
		password = new char[strlen(pwd) + 1];
		strcpy(password, pwd);
	}
}

TZip::~TZip() {
	if (state != NULL) {
		delete state;
	}
	state = NULL;
	if (encbuf != NULL) {
		delete[] encbuf;
	}
	encbuf = NULL;
	if (password != NULL) {
		delete[] password;
	}
	password = NULL;
}

uint TZip::create(void* z, uint len, uint flags) {
	if (hfout != 0 || hmapout != 0 || obuf != 0 || writ != 0 || oerr != ZR_OK || hasputcen) {
		return ZR_NOTINITED;
	}
	//
	if (flags == ZIP_HANDLE) {
		HANDLE hf = (HANDLE)z;
		hfout = hf;
		mustclosehfout = false;
#ifdef DuplicateHandle
		BOOL res = DuplicateHandle(GetCurrentProcess(), hf, GetCurrentProcess(), &hfout, 0, FALSE, DUPLICATE_SAME_ACCESS);
		if (res) {
			mustclosehandle = true;
		}
#endif
		// now we have hfout. Either we duplicated the handle and we close it ourselves
		// (while the caller closes h themselves), or we couldn't duplicate it.
		uint res = SetFilePointer(hfout, 0, 0, FILE_CURRENT);
		ocanseek = (res != 0xFFFFFFFF);
		if (ocanseek) {
			ooffset = res;
		}
		else {
			ooffset = 0;
		}
		return ZR_OK;
	}
	else if (flags == ZIP_FILENAME) {
		const TCHAR *fn = (const TCHAR*)z;
		hfout = CreateFile(fn, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hfout == INVALID_HANDLE_VALUE) {
			hfout = 0;
			return ZR_NOFILE;
		}
		ocanseek = true;
		ooffset = 0;
		mustclosehfout = true;
		return ZR_OK;
	}
	else if (flags == ZIP_MEMORY) {
		uint size = len;
		if (size == 0) {
			return ZR_MEMSIZE;
		}
		if (z != 0) {
			obuf = (char*)z;
		}
		else {
			hmapout = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, size, NULL);
			if (hmapout == NULL) {
				return ZR_NOALLOC;
			}
			obuf = (char*)MapViewOfFile(hmapout, FILE_MAP_ALL_ACCESS, 0, 0, size);
			if (obuf == 0) {
				CloseHandle(hmapout);
				hmapout = 0;
				return ZR_NOALLOC;
			}
		}
		ocanseek = true;
		opos = 0;
		mapsize = size;
		return ZR_OK;
	}
	return ZR_ARGS;
}

uint TZip::sflush(void *param, const char *buf, uint *size) { // static
	if (*size == 0) {
		return 0;
	}
	TZip *zip = (TZip*)param;
	uint writ = zip->write(buf, *size);
	if (writ != 0) {
		*size = 0;
	}
	return writ;
}

uint TZip::swrite(void *param, const char *buf, uint size) { // static
	if (size == 0) {
		return 0;
	}
	TZip *zip = (TZip*)param;
	return zip->write(buf, size);
}

uint TZip::write(const char *buf, uint size) {
	const char *srcbuf = buf;
	if (encwriting) {
		if (encbuf != 0 && encbufsize < size) {
			delete[] encbuf;
			encbuf = 0;
		}
		if (encbuf == 0) {
			encbuf = new char[size * 2];
			encbufsize = size;
		}
		memcpy(encbuf, buf, size);
		for (unsigned int i = 0; i < size; i++) {
			encbuf[i] = zencode(keys, encbuf[i]);
		}
		srcbuf = encbuf;
	}
	if (obuf != 0) {
		if (opos + size >= mapsize) {
			oerr = ZR_MEMSIZE;
			return 0;
		}
		memcpy(obuf + opos, srcbuf, size);
		opos += size;
		return size;
	}
	else if (hfout != 0) {
		DWORD writ;
		WriteFile(hfout, srcbuf, size, &writ, NULL);
		return writ;
	}
	oerr = ZR_NOTINITED;
	return 0;
}

bool TZip::oseek(uint pos) {
	if (!ocanseek) {
		oerr = ZR_SEEK;
		return false;
	}
	if (obuf != 0) {
		if (pos >= mapsize) {
			oerr = ZR_MEMSIZE;
			return false;
		}
		opos = pos;
		return true;
	}
	else if (hfout != 0) {
		SetFilePointer(hfout, pos + ooffset, NULL, FILE_BEGIN);
		return true;
	}
	oerr = ZR_NOTINITED;
	return 0;
}

uint TZip::getMemory(void **pbuf, ulong *plen) {
	if (!hasputcen) {
		addCentral();
	}
	hasputcen = true;
	if (pbuf != NULL) {
		*pbuf = (void*)obuf;
	}
	if (plen != NULL) {
		*plen = writ;
	}
	if (obuf == NULL) {
		return ZR_NOTMMAP;
	}
	return ZR_OK;
}

uint TZip::close() {
	uint res = ZR_OK;
	if (!hasputcen) {
		res = addCentral();
	}
	hasputcen = true;
	if (obuf != 0 && hmapout != 0) {
		UnmapViewOfFile(obuf);
	}
	obuf = 0;
	if (hmapout != 0) {
		CloseHandle(hmapout);
	}
	hmapout = 0;
	if (hfout != 0 && mustclosehfout) {
		CloseHandle(hfout);
	}
	hfout = 0;
	mustclosehfout = false;
	return res;
}

uint TZip::addCentral() { // write central directory
	int numentries = 0;
	ulong pos_at_start_of_central = writ;
	bool okay = true;
	for (TZipFileInfo *zfi = zfis; zfi != NULL;) {
		if (okay) {
			int res = putcentral(zfi, swrite, this);
			if (res != ZE_OK) okay = false;
		}
		writ += 4 + CENHEAD + (uint)zfi->nam + (uint)zfi->cext + (uint)zfi->com;
		
		numentries++;
		
		TZipFileInfo *zfinext = zfi->nxt;
		if (zfi->cextra != 0) {
			delete[] zfi->cextra;
		}
		delete zfi;
		zfi = zfinext;
	}
	ulong center_size = writ - pos_at_start_of_central;
	if (okay) {
		int res = putend(numentries, center_size, pos_at_start_of_central + ooffset, 0, NULL, swrite, this);
		if (res != ZE_OK) {
			okay = false;
		}
		writ += 4 + ENDHEAD + 0;
	}
	if (!okay) {
		return ZR_WRITE;
	}
	return ZR_OK;
}

uint TZip::openFile(const TCHAR *fn)
{
	hfin = 0;
	bufin = 0;
	selfclosehf = false;
	crc = CRCVAL_INITIAL;
	isize = 0; csize = 0;
	ired = 0;
	if (fn == 0) return ZR_ARGS;
	HANDLE hf = CreateFile(fn, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (hf == INVALID_HANDLE_VALUE) {
		return ZR_NOFILE;
	}
	uint res = openHandle(hf, 0);
	if (res != ZR_OK) { 
		CloseHandle(hf); 
		return res; 
	}
	selfclosehf = true;
	return ZR_OK;
}

uint TZip::openHandle(HANDLE hf, uint len)
{
	hfin = 0;
	bufin = 0;
	selfclosehf = false;
	crc = CRCVAL_INITIAL;
	isize = 0;
	csize = 0;
	ired = 0;
	if (hf == 0 || hf == INVALID_HANDLE_VALUE) {
		return ZR_ARGS;
	}
	uint res = SetFilePointer(hfout, 0, 0, FILE_CURRENT);
	if (res != 0xFFFFFFFF)
	{
		uint res = GetFileInfo(hf, &attr, &isize, &times, &timestamp);
		if (res != ZR_OK) {
			return res;
		}
		SetFilePointer(hf, 0, NULL, FILE_BEGIN); // because GetFileInfo will have screwed it up
		iseekable = true; hfin = hf;
		return ZR_OK;
	}

	attr = 0x80000000;      // just a normal file
	isize = -1;            // can't know size until at the end
	if (len != 0) {
		isize = len; // unless we were told explicitly!
	}
	iseekable = false;
	SYSTEMTIME st; GetLocalTime(&st);
	FILETIME ft;   SystemTimeToFileTime(&st, &ft);
	WORD dosdate, dostime;
	filetime2dosdatetime(ft, &dosdate, &dostime);
	times.atime = filetime2timet(ft);
	times.mtime = times.atime;
	times.ctime = times.atime;
	timestamp = (WORD)dostime | (((DWORD)dosdate) << 16);
	hfin = hf;
	return ZR_OK;
}

uint TZip::openMem(void *src, uint len)
{
	hfin = 0;
	bufin = (const char*)src;
	selfclosehf = false;
	crc = CRCVAL_INITIAL;
	ired = 0;
	csize = 0;
	ired = 0;
	lenin = len; posin = 0;
	if (src == 0 || len == 0) {
		return ZR_ARGS;
	}
	attr = 0x80000000; // just a normal file
	isize = len;
	iseekable = true;
	SYSTEMTIME st;
	GetLocalTime(&st);
	FILETIME ft;
	SystemTimeToFileTime(&st, &ft);
	WORD dosdate, dostime;
	filetime2dosdatetime(ft, &dosdate, &dostime);
	times.atime = filetime2timet(ft);
	times.mtime = times.atime;
	times.ctime = times.atime;
	timestamp = (WORD)dostime | (((DWORD)dosdate) << 16);
	return ZR_OK;
}

uint TZip::openDir()
{
	hfin = 0;
	bufin = 0;
	selfclosehf = false;
	crc = CRCVAL_INITIAL;
	isize = 0;
	csize = 0;
	ired = 0;
	attr = 0x41C00010; // a readable writable directory, and again directory
	isize = 0;
	iseekable = false;
	SYSTEMTIME st; GetLocalTime(&st);
	FILETIME ft;   SystemTimeToFileTime(&st, &ft);
	WORD dosdate, dostime; filetime2dosdatetime(ft, &dosdate, &dostime);
	times.atime = filetime2timet(ft);
	times.mtime = times.atime;
	times.ctime = times.atime;
	timestamp = (WORD)dostime | (((DWORD)dosdate) << 16);
	return ZR_OK;
}

uint TZip::sread(TState &s, char *buf, uint size)
{ // static
	TZip *zip = (TZip*)s.param;
	return zip->read(buf, size);
}

uint TZip::read(char *buf, uint size)
{
	if (bufin != 0)
	{
		if (posin >= lenin) return 0; // end of input
		ulong red = lenin - posin;
		if (red>size) red = size;
		memcpy(buf, bufin + posin, red);
		posin += red;
		ired += red;
		crc = crc32(crc, (uch*)buf, red);
		return red;
	}
	else if (hfin != 0)
	{
		DWORD red;
		BOOL ok = ReadFile(hfin, buf, size, &red, NULL);
		if (!ok) return 0;
		ired += red;
		crc = crc32(crc, (uch*)buf, red);
		return red;
	}
	oerr = ZR_NOTINITED; 
	return 0; 
}

uint TZip::iclose()
{
	if (selfclosehf && hfin != 0) {
		CloseHandle(hfin);
		hfin = 0;
	}
	bool mismatch = (isize != -1 && isize != ired);
	isize = ired; // and crc has been being updated anyway
	if (mismatch) {
		return ZR_MISSIZE;
	}
	else return ZR_OK;
}

uint TZip::ideflate(TZipFileInfo *zfi)
{
	if (state == 0) {
		state = new TState();
	}

	state->err = 0;
	state->readfunc = sread; state->flush_outbuf = sflush;
	state->param = this; state->level = 8; state->seekable = iseekable; state->err = NULL;
	// the following line will make ct_init realise it has to perform the init
	state->ts.static_dtree[0].dl.len = 0;
	// Thanks to Alvin77 for this crucial fix:
	state->ds.window_size = 0;
	//  I think that covers everything that needs to be initted.
	//
	bi_init(*state, buf, sizeof(buf), TRUE); // it used to be just 1024-size, not 16384 as here
	ct_init(*state, &zfi->att);
	lm_init(*state, state->level, &zfi->flg);
	ulg sz = deflate(*state);
	csize = sz;
	ZRESULT r = ZR_OK; if (state->err != NULL) r = ZR_FLATE;
	return r;
}

uint TZip::istore()
{
	ulong size = 0;
	for (;;)
	{
		uint cin = read(buf, 16384);
		if (cin <= 0 || cin == (uint)EOF) {
			break;
		}
		uint cout = write(buf, cin);
		if (cout != cin) {
			return ZR_MISSIZE;
		}
		size += cin;
	}
	csize = size;
	return ZR_OK;
}

uint TZip::add(const TCHAR *odstzn, void *src, uint len, DWORD flags)
{
	if (oerr) {
		return ZR_FAILED;
	}
	if (hasputcen) {
		return ZR_ENDED;
	}

	// if we use password encryption, then every isize and csize is 12 bytes bigger
	int passex = 0;
	if (password != 0 && flags != ZIP_FOLDER) {
		passex = 12;
	}

	// zip has its own notion of what its names should look like: i.e. dir/file.stuff
	TCHAR dstzn[MAX_PATH];
	_tcscpy(dstzn, odstzn);
	if (*dstzn == 0) {
		return ZR_ARGS;
	}
	TCHAR *d = dstzn;
	while (*d != 0) { 
		if (*d == '\\') {
			*d = '/';
		}
		d++;
	}
	bool isdir = (flags == ZIP_FOLDER);
	bool needs_trailing_slash = (isdir && dstzn[_tcslen(dstzn) - 1] != '/');
	int method = DEFLATE;
	if (isdir || HasZipSuffix(dstzn)) {
		method = STORE;
	}

	// now open whatever was our input source:
	uint openres;
	switch (flags)
	{
	case ZIP_FILENAME:
		openres = openFile((const TCHAR*)src);
		break;
	case ZIP_HANDLE:
		openres = openHandle((HANDLE)src, len);
		break;
	case ZIP_MEMORY:
		openres = openMem(src, len);
		break;
	case ZIP_FOLDER:
		openres = openDir();
		break;
	default:
		return ZR_ARGS;
	}

	if (openres != ZR_OK) {
		return openres;
	}

	// A zip "entry" consists of a local header (which includes the file name),
	// then the compressed data, and possibly an extended local header.

	// Initialize the local header
	TZipFileInfo zfi;
	zfi.nxt = NULL;
	strcpy(zfi.name, "");
#ifdef UNICODE
	WideCharToMultiByte(CP_UTF8, 0, dstzn, -1, zfi.iname, MAX_PATH, 0, 0);
#else
	strcpy(zfi.iname, dstzn);
#endif
	zfi.nam = strlen(zfi.iname);
	if (needs_trailing_slash) { 
		strcat(zfi.iname, "/"); 
		zfi.nam++; 
	}
	strcpy(zfi.zname, "");
	zfi.extra = NULL; 
	zfi.ext = 0;   // extra header to go after this compressed data, and its length
	zfi.cextra = NULL;
	zfi.cext = 0; // extra header to go in the central end-of-zip directory, and its length
	zfi.comment = NULL;
	zfi.com = 0; // comment, and its length
	zfi.mark = 1;
	zfi.dosflag = 0;
	zfi.att = (ush)BINARY;
	zfi.vem = (ush)0xB17; // 0xB00 is win32 os-code. 0x17 is 23 in decimal: zip 2.3
	zfi.ver = (ush)20;    // Needs PKUNZIP 2.0 to unzip it
	zfi.tim = timestamp;
	// Even though we write the header now, it will have to be rewritten, since we don't know compressed size or crc.
	zfi.crc = 0;            // to be updated later
	zfi.flg = 8;            // 8 means 'there is an extra header'. Assume for the moment that we need it.
	if (password != 0 && !isdir) {
		zfi.flg = 9;  // and 1 means 'password-encrypted'
	}
	zfi.lflg = zfi.flg;     // to be updated later
	zfi.how = (ush)method;  // to be updated later
	zfi.siz = (ulg)(method == STORE && isize >= 0 ? isize + passex : 0); // to be updated later
	zfi.len = (ulg)(isize);  // to be updated later
	zfi.dsk = 0;
	zfi.atx = attr;
	zfi.off = writ + ooffset;         // offset within file of the start of this local record
									  // stuff the 'times' structure into zfi.extra

									  // nb. apparently there's a problem with PocketPC CE(zip)->CE(unzip) fails. And removing the following block fixes it up.
	char xloc[EB_L_UT_SIZE];
	zfi.extra = xloc;
	zfi.ext = EB_L_UT_SIZE;
	char xcen[EB_C_UT_SIZE];
	zfi.cextra = xcen;
	zfi.cext = EB_C_UT_SIZE;
	xloc[0] = 'U';
	xloc[1] = 'T';
	xloc[2] = EB_UT_LEN(3);       // length of data part of e.f.
	xloc[3] = 0;
	xloc[4] = EB_UT_FL_MTIME | EB_UT_FL_ATIME | EB_UT_FL_CTIME;
	xloc[5] = (char)(times.mtime);
	xloc[6] = (char)(times.mtime >> 8);
	xloc[7] = (char)(times.mtime >> 16);
	xloc[8] = (char)(times.mtime >> 24);
	xloc[9] = (char)(times.atime);
	xloc[10] = (char)(times.atime >> 8);
	xloc[11] = (char)(times.atime >> 16);
	xloc[12] = (char)(times.atime >> 24);
	xloc[13] = (char)(times.ctime);
	xloc[14] = (char)(times.ctime >> 8);
	xloc[15] = (char)(times.ctime >> 16);
	xloc[16] = (char)(times.ctime >> 24);
	memcpy(zfi.cextra, zfi.extra, EB_C_UT_SIZE);
	zfi.cextra[EB_LEN] = EB_UT_LEN(1);


	// (1) Start by writing the local header:
	int r = putlocal(&zfi, swrite, this);
	if (r != ZE_OK) { 
		iclose();
		return ZR_WRITE;
	}
	writ += 4 + LOCHEAD + (uint)zfi.nam + (uint)zfi.ext;
	if (oerr != ZR_OK) { 
		iclose(); 
		return oerr;
	}

	// (1.5) if necessary, write the encryption header
	keys[0] = 305419896L;
	keys[1] = 591751049L;
	keys[2] = 878082192L;
	for (const char *cp = password; cp != 0 && *cp != 0; cp++) {
		update_keys(keys, *cp);
	}
	// generate some random bytes
	if (!has_seeded) {
		srand(GetTickCount() ^ (ulong)GetDesktopWindow());
	}
	char encbuf[12]; for (int i = 0; i<12; i++) encbuf[i] = (char)((rand() >> 7) & 0xff);
	encbuf[11] = (char)((zfi.tim >> 8) & 0xff);
	for (int ei = 0; ei<12; ei++) encbuf[ei] = zencode(keys, encbuf[ei]);
	if (password != 0 && !isdir) { swrite(this, encbuf, 12); writ += 12; }

	//(2) Write deflated/stored file to zip file
	ZRESULT writeres = ZR_OK;
	encwriting = (password != 0 && !isdir);  // an object member variable to say whether we write to disk encrypted
	if (!isdir && method == DEFLATE) writeres = ideflate(&zfi);
	else if (!isdir && method == STORE) writeres = istore();
	else if (isdir) csize = 0;
	encwriting = false;
	iclose();
	writ += csize;
	if (oerr != ZR_OK) return oerr;
	if (writeres != ZR_OK) return ZR_WRITE;

	// (3) Either rewrite the local header with correct information...
	bool first_header_has_size_right = (zfi.siz == csize + passex);
	zfi.crc = crc;
	zfi.siz = csize + passex;
	zfi.len = isize;
	if (ocanseek && (password == 0 || isdir))
	{
		zfi.how = (ush)method;
		if ((zfi.flg & 1) == 0) zfi.flg &= ~8; // clear the extended local header flag
		zfi.lflg = zfi.flg;
		// rewrite the local header:
		if (!oseek(zfi.off - ooffset)) return ZR_SEEK;
		if ((r = putlocal(&zfi, swrite, this)) != ZE_OK) return ZR_WRITE;
		if (!oseek(writ)) return ZR_SEEK;
	}
	else
	{ // (4) ... or put an updated header at the end
		if (zfi.how != (ush)method) {
			return ZR_NOCHANGE;
		}
		if (method == STORE && !first_header_has_size_right) {
			return ZR_NOCHANGE;
		}
		if ((r = putextended(&zfi, swrite, this)) != ZE_OK) {
			return ZR_WRITE;
		}
		writ += 16L;
		zfi.flg = zfi.lflg; // if flg modified by inflate, for the central index
	}
	if (oerr != ZR_OK) {
		return oerr;
	}

	// Keep a copy of the zipfileinfo, for our end-of-zip directory
	char *cextra = new char[zfi.cext];
	memcpy(cextra, zfi.cextra, zfi.cext);
	zfi.cextra = cextra;
	TZipFileInfo *pzfi = new TZipFileInfo;
	memcpy(pzfi, &zfi, sizeof(zfi));
	if (zfis == NULL) {
		zfis = pzfi;
	}
	else { 
		TZipFileInfo *z = zfis;
		while (z->nxt != NULL) {
			z = z->nxt;
		}
		z->nxt = pzfi;
	}
	return ZR_OK;
}