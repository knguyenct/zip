#include "zip.h"

HZIP createZipInternal(void *z, uint len, DWORD flags, const char *password) {
	TZip *zip = new TZip(password);

	uint lasterrorZ = zip->create(z, len, flags);
	if (lasterrorZ != ZR_OK) {
		delete zip;
		return 0;
	}

	TZipHandleData *han = new TZipHandleData;
	han->flag = 2;
	han->zip = zip;
	return (HZIP)han;
}

HZIP createZipHandle(HANDLE h, const char *password) {
	return createZipInternal(h, 0, ZIP_HANDLE, password);
}

HZIP createZip(const TCHAR *fn, const char *password) {
	return createZipInternal((void*)fn, 0, ZIP_FILENAME, password);
}

HZIP createZip(void *z, uint len, const char *password) {
	return createZipInternal(z, len, ZIP_MEMORY, password);
}

uint zipAddInternal(HZIP hz, const TCHAR *dstzn, void *src, uint len, DWORD flags) {
	if (hz == 0) {
		return ZR_ARGS;
	}
	TZipHandleData *han = (TZipHandleData*)hz;
	if (han->flag != 2) {
		return ZR_ZMODE;
	}
	TZip *zip = han->zip;
	return zip->add(dstzn, src, len, flags);
}

uint zipAdd(HZIP hz, const TCHAR *dstzn, const TCHAR *fn) {
	return zipAddInternal(hz, dstzn, (void*)fn, 0, ZIP_FILENAME);
}

uint zipAdd(HZIP hz, const TCHAR *dstzn, void *src, uint len) {
	return zipAddInternal(hz, dstzn, src, len, ZIP_MEMORY);
}

uint zipAddHandle(HZIP hz, const TCHAR *dstzn, HANDLE h) {
	return zipAddInternal(hz, dstzn, h, 0, ZIP_HANDLE);
}

uint zipAddHandle(HZIP hz, const TCHAR *dstzn, HANDLE h, uint len) {
	return zipAddInternal(hz, dstzn, h, len, ZIP_HANDLE);
}

uint zipAddFolder(HZIP hz, const TCHAR *dstzn) {
	return zipAddInternal(hz, dstzn, 0, 0, ZIP_FOLDER);
}

uint zipGetMemory(HZIP hz, void **buf, ulong *len) {
	if (hz == 0) {
		if (buf != 0) {
			*buf = 0;
		}
		if (len != 0) {
			*len = 0;
		}
		return ZR_ARGS;
	}
	TZipHandleData *han = (TZipHandleData*)hz;
	if (han->flag != 2) {
		return ZR_ZMODE;
	}
	TZip *zip = han->zip;
	return zip->getMemory(buf, len);
	;
}

uint closeZip(HZIP hz) {
	if (hz == 0) {
		return ZR_ARGS;
	}
	TZipHandleData *han = (TZipHandleData*)hz;
	if (han->flag != 2) {
		return ZR_ZMODE;
	}
	TZip *zip = han->zip;
	uint res = zip->close();
	delete zip;
	delete han;
	return res;
}

uint formatZipMessage(uint code, TCHAR *buf, uint len) {
	const char *msg = "unknown zip result code";
	switch (code) {
	case ZR_OK:
		msg = "Success";
		break;
	case ZR_NODUPH:
		msg = "Culdn't duplicate handle";
		break;
	case ZR_NOFILE:
		msg = "Couldn't create/open file";
		break;
	case ZR_NOALLOC:
		msg = "Failed to allocate memory";
		break;
	case ZR_WRITE:
		msg = "Error writing to file";
		break;
	case ZR_NOTFOUND:
		msg = "File not found in the zipfile";
		break;
	case ZR_MORE:
		msg = "Still more data to unzip";
		break;
	case ZR_CORRUPT:
		msg = "Zipfile is corrupt or not a zipfile";
		break;
	case ZR_READ:
		msg = "Error reading file";
		break;
	case ZR_ARGS:
		msg = "Caller: faulty arguments";
		break;
	case ZR_PARTIALUNZ:
		msg = "Caller: the file had already been partially unzipped";
		break;
	case ZR_NOTMMAP:
		msg = "Caller: can only get memory of a memory zipfile";
		break;
	case ZR_MEMSIZE:
		msg = "Caller: not enough space allocated for memory zipfile";
		break;
	case ZR_FAILED:
		msg = "Caller: there was a previous error";
		break;
	case ZR_ENDED:
		msg = "Caller: additions to the zip have already been ended";
		break;
	case ZR_ZMODE:
		msg = "Caller: mixing creation and opening of zip";
		break;
	case ZR_NOTINITED:
		msg = "Zip-bug: internal initialisation not completed";
		break;
	case ZR_SEEK:
		msg = "Zip-bug: trying to seek the unseekable";
		break;
	case ZR_MISSIZE:
		msg = "Zip-bug: the anticipated size turned out wrong";
		break;
	case ZR_NOCHANGE:
		msg = "Zip-bug: tried to change mind, but not allowed";
		break;
	case ZR_FLATE:
		msg = "Zip-bug: an internal error during flation";
		break;
	}
	uint mlen = (uint)strlen(msg);
	if (buf == 0 || len == 0) {
		return mlen;
	}
	int n = mlen;
	if (n + 1 > len) {
		n = len - 1;
	}
	strncpy(buf, msg, n);
	buf[n] = 0;
	return mlen;
}
