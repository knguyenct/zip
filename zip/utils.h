#ifndef UTILS_H
#define UTILS_H

#include <Windows.h>

#include "const.h"
#include "struct.h"
#include "tstate.h"

int putend(int n, ulong s, ulong c, extent m, char *z, WRITEFUNC wfunc, void *param);

#endif