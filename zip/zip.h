#ifndef ZIP_H
#define ZIP_H

#include <tchar.h>
#include <Windows.h>

#include "struct.h"
#include "tzip.h"
#include "zip_const.h"

#ifndef UNZIP_H
DECLARE_HANDLE(HZIP);
#endif

typedef struct
{
	DWORD flag;
	TZip *zip;
} TZipHandleData;

HZIP createZip(const TCHAR *fn, const char *password);

HZIP createZip(void *buf, uint len, const char *password);

HZIP createZipHandle(HANDLE h, const char *password);

uint zipAdd(HZIP hz, const TCHAR *dstzn, const TCHAR *fn);

uint zipAdd(HZIP hz, const TCHAR *dstzn, void *src, uint len);

uint zipAddHandle(HZIP hz, const TCHAR *dstzn, HANDLE h);

uint zipAddHandle(HZIP hz, const TCHAR *dstzn, HANDLE h, uint len);

uint zipAddFolder(HZIP hz, const TCHAR *dstzn);

uint zipGetMemory(HZIP hz, void **buf, ulong *len);

uint closeZip(HZIP hz);

uint formatZipMessage(uint code, TCHAR *buf, uint len);

#endif